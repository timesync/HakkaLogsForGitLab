from HakkaLogsForGitLab import app

from json_transform_view import JsonTransformView

jtv = JsonTransformView.as_view('user_api')

# Main web pages for ADP Metrics
app.add_url_rule('/log', 'log', view_func=jtv, methods=["POST"])
