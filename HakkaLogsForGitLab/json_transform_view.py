from HakkaLogsForGitLab import app, logger

from flask import render_template, render_template_string, json, request, abort
from flask.views import MethodView

import requests

# Ugly Python hack to make SSL work properly with broken SSL servers and Ubuntu 12.04
# Thanks: https://gist.github.com/LordGaav/4674198
try:
    # Ugly hack to force SSLv3 and avoid
    # urllib2.URLError: <urlopen error [Errno 1] _ssl.c:504: error:14077438:SSL routines:SSL23_GET_SERVER_HELLO:tlsv1 alert internal error>
    import _ssl
    _ssl.PROTOCOL_SSLv23 = _ssl.PROTOCOL_TLSv1
except:
    pass

class JsonTransformView(MethodView):
    '''
    JsonTransform()

    The transformation view object taking in json from GitLab and
    outputting the appropriate Json formatted object for HakkaLogsForGitlab.
    '''
    def __init__(self):
        super(JsonTransformView, self).__init__()

        logger.info('[JsonTransformView] Instantiated with request and APIs.')

    def post(self):
        logs_response = []
        log = {}
        hakkalogs_url = 'https://www.hakkalabs.co/api/webhooks?service=' + request.args.get('service') + '&token=' + request.args.get('token')
        headers = {'Content-Type': 'application/json'}

        logger.info('[JsonTransformView] Posting to ' + hakkalogs_url)
        logger.info('[JsonTransformView] Data: ' + json.dumps(request.json))

        if request.headers['Content-Type'] == 'application/json':
            # This only works with GitLab. However, it can be branched out to 
            # work with other git repositories.
            for commit in request.json['commits']:
                if '[log]' in commit['message']:
                    logs_response.append(commit['message'].replace('[log]', '').strip())

            # Still need to figure out the proper way to deal with multiple commit
            # messages that are sent in one push.
            if len(logs_response) > 0:
                log = {
                    "log": " ".join(logs_response),
                    "state": "private"
                }

                logger.info('[JsonTransformView] Log: ' + json.dumps(log))

                r = requests.post(hakkalogs_url, headers=headers, json=log, allow_redirects=True, verify=False)
                return json.dumps(r.text)
            else:
                return json.dumps({})
        else:
            abort(415)
