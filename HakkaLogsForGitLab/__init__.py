'''
__init__.py

	The initialization file for the Flask application.

	In this file the Flask application is initialized
	from both a default_settings object as well as a
	config file. Afterwards, logging and views are set
	up.
'''

from flask import Flask

import logging

from default_settings import default_settings


# Setup Flask
app = Flask(__name__)

# Setup the application
app.config.from_object(default_settings)
app.config.from_pyfile('instance/config.py', silent=True)
app.config.from_envvar('HL4GL_CONFIG_FILE', silent=True)

# Set up logging
logger = logging.getLogger(app.logger_name)
logger.setLevel(eval(app.config['LOG_LEVEL']) if app.config['LOG_LEVEL'] else logging.DEBUG)

# create file handler which logs even debug messages
fh = logging.FileHandler(app.config['LOG_FILE'] if app.config['LOG_FILE'] else app.config['/tmp/hl4gl.log'])
fh.setLevel(logging.INFO)

# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)

# add the handlers to the logger
logger.addHandler(fh)

# Import the views
import views
