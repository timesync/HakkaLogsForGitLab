# Configuration
DEBUG = True
HOST = "127.0.0.1"
PORT = 5001
LOGGER_NAME = 'hl4gl'
LOG_FILE = '/tmp/hl4gl.log'
LOG_LEVEL = 'logging.INFO'
