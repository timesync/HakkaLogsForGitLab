# Hakka Logs For GitLab Webhook

Hakka Logs is a tool for developers to use to keep track of what they're up to. Check out the [Hakka Logs FAQ]([https://www.hakkalabs.co/logs/faq]) if you have any questions.

## Usage

This webhook is written specifically for integrating GitLab with Hakka Logs. There is a working version of this webhook running on Heroku. If you don't care about running your own instance of this, then you can go ahead and do the following:

1. Go to the _Web Hooks_ settings section for the GitLab project you care about (e.g. - https://gitlab.com/timesync/HakkaLogsForGitLab/hooks)

2. Paste the following URL in to the webhook box: https://hakka-logs-for-gitlab.herokuapp.com/log?service=custom&token=_your-hakka-logs-webhook-token_

## Installation

If you want to install this in your own environment, then just clone the repo and go about your business. Enjoy yourself.

## FAQ

* **If I use your Heroku instance, won't you see my Hakka Logs webhook token in your log file?** Yes. I promise I won't do anything nefarious with it.

* **Is this under active development?** Yes. 

* **Can I contribute?** Yes. Feel free to tweet me: [@timesync](http://www.twitter.com/@timesync)
